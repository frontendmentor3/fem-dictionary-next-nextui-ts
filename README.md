# Frontend Mentor - Dictionary web app solution

This is a solution to the [Dictionary web app challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/dictionary-web-app-h5wwnyuKFL). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
    - [The challenge](#the-challenge)
    - [Screenshot](#screenshot)
    - [Links](#links)
- [My process](#my-process)
    - [Built with](#built-with)
    - [What I learned](#what-i-learned)
    - [Continued development](#continued-development)
    - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

-[x] Search for words using the input field
-[x] See the Free Dictionary API's response for the searched word
-[x] See a form validation message when trying to submit a blank form
-[x] Play the audio file for a word when it's available
-[x] Switch between serif, sans serif, and monospace fonts
-[x] Switch between light and dark themes
-[x] View the optimal layout for the interface depending on their device's screen size
-[x] See hover and focus states for all interactive elements on the page
-[x] **Bonus**: Have the correct color scheme chosen for them based on their computer preferences. _Hint_: Research `prefers-color-scheme` in CSS.

### Screenshot
[![](https://i.imgur.com/VN4S5Vrm.jpg)](https://i.imgur.com/VN4S5Vr.png)
[![](https://i.imgur.com/SDE1EDtm.jpg)](https://i.imgur.com/SDE1EDt.png)
[![](https://i.imgur.com/U6kDXA9m.jpg)](https://i.imgur.com/U6kDXA9.png)
<br/>
Dark and light themes, play button only shows when there's audio available

### Links

- Solution URL: [Gitlab](https://gitlab.com/frontendmentor3/fem-dictionary-next-nextui-ts)
- Live Site URL: [Netlify](https://singular-froyo-f3b589.netlify.app/)

## My process

### Built with

- [Next.js](https://nextjs.org/) - React framework
- [NextUI](https://nextui.org/) - For styles


### What I learned

#### Importing SVG as a component to change color etc
```ts
<MoonIcon stroke={"var(--nextui-colors-moonColor)"}/>
```

#### Using multiple google fonts with nextui
1. Use @next/font/google, create css variables, and import in app.layout.tsx
   (https://beta.nextjs.org/docs/optimizing/fonts#using-multiple-fonts)
2. Override the default font or a theme, in NextUI
  ```ts
  theme:{
  colors:{...},
  fonts:{
    sans:'var(--font-inter)', 
    mono:'var(--font-inconsolata)',
  }
}
```
3. use it like so,
```ts
// app/layout.tsx
<html lang="en"
className={`${inter.variable} ${inconsolata.variable} ${lora.variable}`}
>

// some other files
<Text color='$secondary' css={{
  fontFamily:'$sans',
  fontWeight: 'bold',
  letterSpacing: '1px'
}}>{phonetic}</Text>

// another example
<Container css={{fontFamily:'$mono'}}>
```

#### Use theme color,
```ts
<MoonIcon stroke={"var(--nextui-colors-switchButton)"}/>
```

#### Change Images on mouseover with img tag
```ts
<Image src="assets/images/icon-play.svg" alt="playIcon" width={48} height={48}
       onMouseOver={e=>e.currentTarget.src="assets/images/icon-play-hover.svg"}
       onMouseOut={e=>e.currentTarget.src="assets/images/icon-play.svg"}
       style={{cursor:'pointer'}}
/>
```

```ts
 css={{
  '--nextui--switchWidth': 'var(--nextui-space-14)',
  '.nextui-switch-circle':{
      backgroundColor: '$switchButton'
  }
```

#### matchMedia

example
```ts
const prefersDark = window.matchMedia(
    "(prefers-color-scheme: dark)"
  ).matches;
```

https://stackoverflow.com/questions/61117608/how-do-i-set-system-preference-dark-mode-in-a-react-app-but-also-allow-users-to

### Continued development and Issues
Since NextUI is still in beta there are a few things that are not implemented or don't seem to work properly
- NextUI doesn't support lists yet
- can't overwrite default p style, globalCss doesn't seem to work
<br/>
[![](https://i.imgur.com/IHjEqWHm.jpg)](https://i.imgur.com/IHjEqWH.png)
<br/>
https://github.com/nextui-org/nextui/discussions/908#discussioncomment-4875035
https://nextui.org/docs/theme/customize-theme#global-styles

- onContentClick doesn't seem to work
```ts
onContentClick={()=>console.log('content click')}
```
- cannot change switch background when it's off, using css to set background will give it a square border
- "Initial Checked" property doesn't seem to work, it's always unchecked when a variable is passed in, 
  but works when true/false is passed in directly, alternative solution is to just use 'checked'
  https://nextui.org/docs/components/switch#initial-cheked

#### Improvements
- loop through phonetics and search for audio and use the entry with audio 
- fix FOUC

### Useful resources

- [NextUI next13 provider issue](https://github.com/nextui-org/nextui/issues/849)
- [Next.js 13 third-party context providers](https://beta.nextjs.org/docs/rendering/server-and-client-components#rendering-third-party-context-providers-in-server-components) 
- [Getting started with NextUI and Next.js](https://blog.logrocket.com/getting-started-nextui-next-js/)


## Author

- Github - [cherylli](https://github.com/cherylli)
- Gitlab - [cherylm](https://gitlab.com/cherylm)
- Frontend Mentor - [@cherylli](https://www.frontendmentor.io/profile/cherylli)

