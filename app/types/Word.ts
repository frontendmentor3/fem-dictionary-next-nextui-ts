interface Definition {
    definition: string,
    synonyms: string[],
    antonyms: string[],
    example?: string
}

export interface Meaning {
    partOfSpeech: string,
    definitions: Definition[]
    synonyms: string[],
    antonyms: string[]
}

export interface Phonetic {
    text: string,
    audio: string,
    sourceUrl?: string
}

export interface Word {
    word: string,
    phonetic: string,
    phonetics: Phonetic[]
    meanings: Meaning[]
    license: {
        name: string,
        url: string
    }
    sourceUrls: string[],
}