"use client"

import {SetStateAction, useState} from 'react';
import {Container, Input} from '@nextui-org/react';
import SearchIcon from '@/app/icons/SearchIcon';
import {Word} from '@/app/types/Word';
import WordComponent from '@/app/components/Word'
import Meanings from '@/app/components/Meanings';
import Sources from '@/app/components/Sources';
//import {globalStyles} from '@/app/styles/global';
import NotFound from '@/app/components/NotFound';

export default function Home() {
    // doesn't seem to work
    // https://github.com/nextui-org/nextui/discussions/908#discussioncomment-4875035
    // https://nextui.org/docs/theme/customize-theme#global-styles
    //globalStyles('$mono')
    const [searchResult, setSearchResult] = useState<Word | null>(null)
    const [searchTerm, setSearchTerm] = useState<string>('')
    const [found, setFound] = useState<boolean | null>(null)


    const searchWord = async (term:string="") => {
        if(term!==''){
            setSearchTerm(term)
        }
        try {
            if (searchTerm !== '') {
                const res = await fetch(`https://api.dictionaryapi.dev/api/v2/entries/en/${term===""?searchTerm:term}`)
                if (res.ok) {
                    const result = await res.json()
                    setFound(true)
                    setSearchResult(result[0])
                } else {
                    setFound(false)
                    setSearchResult(null)
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    const handleKeyDown = (e: { key: string; }) => {
        if (e.key === 'Enter') {
            searchWord()
        }
    }

    const handleChange = (e: { target: { value: SetStateAction<string>; }; }) => {
        setSearchTerm(e.target.value)
    }
    return (

        <Container responsive css={{height: "100vh"}}>
            <Input
                placeholder="Search for any words..."
                bordered
                animated={false}
                color="secondary"
                width="100%"
                size="xl"
                css={{marginBottom: '30px', marginTop: '10px'}}
                contentRight={
                    <SearchIcon/>
                }
                helperText={(!searchTerm && found !== null) ? "Whoops, can't be empty..." : ""}
                helperColor="error"
                value={searchTerm}
                onChange={handleChange}
                onKeyDown={handleKeyDown}
                onClick={()=>searchWord(searchTerm)}
                onContentClick={() => console.log('content click')} //not working
            />
            {(searchResult && found) ? (
                <>
                    <WordComponent
                        word={searchResult.word}
                        phonetic={searchResult.phonetic}
                        phonetics={searchResult.phonetics}
                    />
                    <Meanings meanings={searchResult.meanings} searchByTerm={searchWord}/>
                    <Sources sourceUrls={searchResult.sourceUrls}/>
                </>
            ) : found !== null
                ? <NotFound/>
                : null
            }
        </Container>
    )


}
