type IconProps = {
    bgFill?: string
    bgOpacity?: number
    arrowFill?: string
}
const MoonIcon = ({
  bgFill = "#A445ED",
  bgOpacity =0.25,
  arrowFill="#A445ED"
}:IconProps) => {
    return(
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="75"
            height="75"
            viewBox="0 0 75 75">
            <g fill={bgFill} fillRule="evenodd">
                <circle cx="37.5" cy="37.5" r="37.5" opacity={bgOpacity}/>
                <path d="M29 27v21l21-10.5z" fill={arrowFill}/>
            </g>
        </svg>
    )
 }

 export default MoonIcon