import {createTheme} from '@nextui-org/react';
import {customColors} from '@/app/styles/colors';

export const darkTheme = createTheme({
    type: "dark",
    theme:{
        colors:{
            primary: customColors.shared.primary,
            secondary: customColors.shared.primary,
            hr: customColors.shared.hr,
            subtitleGray: customColors.shared.subtitleGray,
            switchButton: customColors.darkTheme.switchButton,
            moonColor: customColors.shared.primary
        },
        fonts:{
            sans:'var(--font-inter)',
            serif: 'var(--font-lora)',
            mono:'var(--font-inconsolata)',
        }
    }
})