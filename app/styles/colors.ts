export const customColors = {
    shared:{
        primary:'#A445ED',
        secondary: '#A445ED',
        hr: '#3A3A3A',
        subtitleGray: '#757575'
    },
    darkTheme:{
        switchButton: '#FFFFFF'
    },
    lightTheme:{
        switchButton: '#A445ED'
    }
}