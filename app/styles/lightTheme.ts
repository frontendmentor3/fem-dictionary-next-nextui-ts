import {createTheme} from '@nextui-org/react';
import {customColors} from '@/app/styles/colors';

export const lightTheme = createTheme({
    type: "light",
    theme:{
        colors:{
            secondary: customColors.shared.secondary,
            hr: customColors.shared.hr,
            subtitleGray: customColors.shared.subtitleGray,
            switchButton: customColors.lightTheme.switchButton,
            moonColor: customColors.shared.subtitleGray
        },
        fonts:{
            sans:'var(--font-inter)',
            serif: 'var(--font-lora)',
            mono:'var(--font-inconsolata)',
        }
    }
})