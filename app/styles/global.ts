import {globalCss} from '@nextui-org/react'


export const globalStyles = (font:string = '$mono') => globalCss({
    p:{
        fontFamily: font
    }
})