import {Inconsolata, Inter, Lora} from '@next/font/google';

export const inconsolata = Inconsolata({
    weight: ['400','700'],
    subsets: ['latin'],
    variable: '--font-inconsolata',
    display: 'swap'
})

export const inter = Inter({
    weight: ['400','700'],
    subsets: ['latin'],
    variable: '--font-inter',
    display: 'swap'
})

export const lora = Lora({
    weight: ['400','700'],
    subsets: ['latin'],
    variable: '--font-lora',
    display: 'swap'
})