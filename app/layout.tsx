"use client"
import {inter, inconsolata, lora} from '@/app/styles/fonts';
import {NextUIProvider, Container} from '@nextui-org/react';
import NavbarComponent from '@/app/components/NavbarComponent';
import {darkTheme} from '@/app/styles/darkTheme';
import {useEffect, useState} from 'react';
import {lightTheme} from '@/app/styles/lightTheme';



export default function RootLayout({children}: {
    children: React.ReactNode
}) {
    const [theme, setTheme] = useState(lightTheme)
    const [font, setFont] = useState("mono")
    const [themeSwitcher, setThemeSwitcher] = useState(false) // initial value of theme switcher

    const toggleTheme = () => {
        if(theme===darkTheme){
            setTheme(lightTheme)
        }else{
            setTheme(darkTheme)
        }
    }

    useEffect(()=>{
        const prefersDark = window.matchMedia(
            "(prefers-color-scheme: dark)"
        ).matches;

        if(prefersDark){
            setTheme(darkTheme)
            setThemeSwitcher(true)
        }
    },[])


    return (

        <html lang="en"
              className={`${inter.variable} ${inconsolata.variable} ${lora.variable}`}
        >
        <head/>
        <body>
        <NextUIProvider theme={theme}>
            <Container css={{
                fontFamily:`$${font}`,
                maxWidth: '600px',

                '@md':{
                    maxWidth:'800px'
                }}
            }
            >
                <NavbarComponent
                    toggleTheme = {toggleTheme}
                    setPageFont={setFont}
                    themeSwitcherChecked={themeSwitcher}
                />
                <main>{children}</main>
            </Container>
        </NextUIProvider>
        </body>
        </html>

    )
}
