import {Meaning} from '@/app/types/Word';
import {Grid, Spacer, Text} from '@nextui-org/react';
import styles from '@/app/styles/lists.module.css'
import Link from 'next/link';

type MeaningsProps = {
    meanings: Meaning[]
    searchByTerm: (term: string) => void
}
const Meanings = ({meanings,searchByTerm}: MeaningsProps) => {
    return (
        <div>
            {meanings.map((meaning, i) => (
                <div key={`$meaning-${i}`}>
                    <Grid.Container justify="space-between" css={{marginTop: '30px'}}>
                        <Grid>
                            <Text span size={18} weight="bold">{meaning.partOfSpeech}</Text>
                        </Grid>
                        <Grid>
                            <Spacer css={{
                                width: '266px',
                                borderTop: '1px solid $hr',
                                marginBottom: '20px'
                            }}/>
                        </Grid>
                    </Grid.Container>
                    <Text span color="$subtitleGray">Meaning</Text>
                    <ul>
                        {meaning.definitions.map((def, i) => (
                            <>
                                <li className={styles.styledLists}
                                    key={`definition-${i}`}>
                                    <Text span size={15}>{def.definition}</Text>
                                </li>
                                {def.example
                                    ? <li><Text span color='$subtitleGray'>
                                        {`"${def.example}"`}
                                    </Text></li>
                                    : null
                                }
                            </>
                        ))}
                    </ul>
                    {meaning.synonyms.length !== 0
                        ? <Grid.Container justify="flex-start">
                            <Grid>
                                <Text span color="$subtitleGray">Synonyms</Text>
                            </Grid>
                            <Grid css={{marginLeft: '40px'}}>
                                {meaning.synonyms.map(s => (
                                    <>
                                        <Text
                                            onClick={()=>searchByTerm(s)}
                                            span color="$secondary"
                                            css={{
                                                cursor: 'pointer',
                                                '&:hover': {
                                                    textDecoration: 'underline'
                                                }
                                            }}>
                                            {s}
                                        </Text>
                                        <br/>
                                    </>
                                ))}
                            </Grid>
                        </Grid.Container>
                        : null
                    }
                </div>
            ))}
        </div>
    )
}

export default Meanings