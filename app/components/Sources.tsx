import {Container, Grid, Spacer, Text} from '@nextui-org/react';
import Image from 'next/image';
import Link from 'next/link';

type SourceSprops = {
    sourceUrls: string[]
}

const Sources = ({sourceUrls}:SourceSprops) => {
    return(
        <div>
            <Container css={{
                width: '100%',
                borderTop: '1px solid $hr',
                marginBottom: '20px',
                marginTop: '30px'
            }}/>
            <Grid.Container direction="column">
                <Grid>
                    <Text span color="$subtitleGray" css={{
                        textDecoration: 'underline',
                        textUnderlineOffset: '3px',
                    }}>Source</Text>
                    <Spacer/>
                </Grid>
                {sourceUrls.map((url,i)=>(
                    <Grid key={i}>
                        <Text span css={{
                            marginTop: '$5',
                            marginRight: '$5'
                        }}>{url}</Text>
                        <Link href={url} target="_blank" rel="noopener noreferrer" >
                            <Image src="assets/images/icon-new-window.svg"
                                   alt="link-icon"
                                   width={12}
                                   height={12}
                            />
                        </Link>

                    </Grid>
                ))}
            </Grid.Container>
            <Spacer/>
        </div>
    )
 }
 
 export default Sources