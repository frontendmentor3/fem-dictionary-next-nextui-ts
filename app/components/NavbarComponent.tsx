"use client"
import Image from 'next/image';
import {Switch, Navbar, Dropdown, Grid} from '@nextui-org/react';
import {DownArrowIcon} from '@/app/icons/DownArrowIcon';
import MoonIcon from '@/app/icons/MoonIcon';
import {useState} from 'react';

type NavProps = {
    toggleTheme: ()=>void
    setPageFont: (font: string)=>void
    themeSwitcherChecked: boolean
}

const NavbarComponent = ({toggleTheme, setPageFont, themeSwitcherChecked}:NavProps) => {
    //console.log(themeSwitcherChecked)
    const [selectedFont, setSelectedFont] = useState("mono")
    const fontSelection = [
        { key: "sans", name:"Sans Serif", family: "sans"},
        { key: "serif", name:"Serif", family: "serif"},
        { key: "mono", name:"Mono", family: "mono"},
    ]

    const getFontName = (key:string)=>{
        return fontSelection.find(o=>o.key === key)?.name
    }


    const handleDropdownAction = (key:string)=>{
        setSelectedFont(key) // set Local state for Display
        setPageFont(key) // set top level font state
    }

    return(
        <Navbar disableShadow>
            <Navbar.Brand>
                <Image src="/assets/images/logo.svg" width={28} height={32} alt="logo"/>
            </Navbar.Brand>
            <Navbar.Content>
                <Dropdown>
                    <Dropdown.Button light icon={<DownArrowIcon/>}>{getFontName(selectedFont)}</Dropdown.Button>
                    <Dropdown.Menu
                        aria-label="Single selection actions"
                        items={fontSelection}
                        // @ts-ignore
                        onAction={handleDropdownAction}
                        selectedKeys={selectedFont}
                        selectionMode="single"
                    >
                        {item=>(
                            // @ts-ignore
                            <Dropdown.Item key={item.key}>{item.name}</Dropdown.Item>
                        )}
                    </Dropdown.Menu>
                </Dropdown>
                <Grid css={{
                    borderLeft:'1px solid $white',
                    paddingLeft: '20px'
                }}>
                    <Switch
                        onChange={toggleTheme}
                        color="secondary"
                        checked={themeSwitcherChecked}
                        bordered
                        size="xs"
                        css={{
                            '--nextui--switchWidth': 'var(--nextui-space-14)',
                            '.nextui-switch-circle':{
                                backgroundColor: '$switchButton'
                            }
                        }}
                    />
                </Grid>

                <MoonIcon stroke={"var(--nextui-colors-moonColor)"}/>
            </Navbar.Content>
        </Navbar>
    )
 }
 
 export default NavbarComponent