import {Grid, Text} from '@nextui-org/react';
import Image from 'next/image';

const NotFound = () => {
    return(
        // @ts-ignore
        <Grid.Container direction="column" align="center" gap={2}>
            <Grid>
                <Text size={50}>😕</Text>
            </Grid>
            <Grid>
                <Text weight="bold" span>No Definition Found</Text>
            </Grid>
            <Grid>
                <Text span color='$subtitleGray'>
                    Sorry pal, we couldn&apos;t find definitions for the word you were looking for. You can try the search again at later time or head to the web instead.</Text>
            </Grid>
        </Grid.Container>
    )
 }
 
 export default NotFound