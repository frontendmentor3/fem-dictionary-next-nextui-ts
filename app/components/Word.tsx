"use client"

import {Grid, Text} from '@nextui-org/react';
import Image from 'next/image';
import {Phonetic} from '@/app/types/Word';


type WordProps = {
    word: string
    phonetic: string
    phonetics: Phonetic[]
}
const Word = ({word, phonetic, phonetics}: WordProps) => {

    const audioLink = phonetics?.find(p=>p.audio!=="")?.audio

    const playAudio = () => {
        const audio = new Audio(audioLink)
        audio.play()
    }

    return (
        <div>
            <Grid.Container justify="space-between" alignItems="center">
                <Grid>
                    <Text h1 size={32} weight="bold" css={{letterSpacing: '1px'}}>{word}</Text>
                    <Text color='$secondary' css={{
                        fontFamily: '$sans',
                        fontWeight: 'bold',
                        letterSpacing: '1px'
                    }}>{phonetic}</Text>
                </Grid>
                <Grid>
                    {audioLink ?
                        <Image src="assets/images/icon-play.svg" alt="playIcon" width={48} height={48}
                               onMouseOver={e => e.currentTarget.src = "assets/images/icon-play-hover.svg"}
                               onMouseOut={e => e.currentTarget.src = "assets/images/icon-play.svg"}
                               style={{cursor: 'pointer'}}
                               onClick={playAudio}
                        /> :
                        null
                    }
                </Grid>
            </Grid.Container>
        </div>
    )
}

export default Word